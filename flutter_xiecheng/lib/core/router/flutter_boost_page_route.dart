
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_boost/boost_navigator.dart';
import 'package:flutter_xiecheng/core/third_party/dialog_page.dart';
import 'package:flutter_xiecheng/core/third_party/lifecycle_test_page.dart';
import 'package:flutter_xiecheng/core/third_party/setting.dart';
import 'package:flutter_xiecheng/core/widget/webview.dart';
import 'package:flutter_xiecheng/page/home/home_page.dart';
import 'package:flutter_xiecheng/page/main/main_page.dart';
import 'package:flutter_xiecheng/page/search/search_page.dart';
import 'package:flutter_xiecheng/page/search/speak_page.dart';

class FlutterBoostRouter {
  /// 由于很多同学说没有跳转动画，这里是因为之前exmaple里面用的是 [PageRouteBuilder]，
  /// 其实这里是可以自定义的，和Boost没太多关系，比如我想用类似iOS平台的动画，
  /// 那么只需要像下面这样写成 [CupertinoPageRoute] 即可
  /// (这里全写成[MaterialPageRoute]也行，这里只不过用[CupertinoPageRoute]举例子)
  ///
  /// 注意，如果需要push的时候，两个页面都需要动的话，
  /// （就是像iOS native那样，在push的时候，前面一个页面也会向左推一段距离）
  /// 那么前后两个页面都必须是遵循CupertinoRouteTransitionMixin的路由
  /// 简单来说，就两个页面都是CupertinoPageRoute就好
  /// 如果用MaterialPageRoute的话同理
  static Map<String, FlutterBoostRouteFactory> routerMap = {
    'mainPage': (settings, uniqueId) {
      return MaterialPageRoute(
          settings: settings,
          builder: (_) {
            //Map<String, Object> map = settings.arguments;
            //String data = map['data'];
            return JKMainScreen(
              data: "data",
            );
          });
    },
    /// web界面
    'webview': (settings, uniqueId) {
      return MaterialPageRoute(
          settings: settings,
          builder: (ctx) {
            Map<String, Object> map = settings.arguments;
            return JKWebView(
              hideAppBar: map['hideAppBar'],
              icon: map['icon'],
              statusBarColor: map['statusBarColor'],
              title: map['title'],
              url: map['url'],
            );
          });
    },

    /// 搜索
    'search_screen': (settings, uniqueId) {
      return MaterialPageRoute(
          settings: settings,
          builder: (ctx) {
            Map<String, Object> map = settings.arguments;
            return SearchPage(
              hideLeft: map['hideLeft'] ?? false,
              keyword: map['keyword'] ?? "",
              hint: map['hint'] ?? "",
            );
          });
    },

    /// 语音搜索界面
    'speak_page': (settings, uniqueId) {
      return MaterialPageRoute(
          settings: settings,
          builder: (ctx) {
            return SpeakPage();
          });
    },

    /// 设置
    'setting': (settings, uniqueId) {
      return MaterialPageRoute(
          settings: settings,
          builder: (ctx) {
            //Map<String, Object> map = settings.arguments;
            return SettingScreen();
          });
    },

    ///生命周期例子页面
    'lifecyclePage': (settings, uniqueId) {
      return CupertinoPageRoute(
          settings: settings,
          builder: (ctx) {
            return LifecycleTestPage();
          });
    },

    ///透明弹窗页面
    'dialogPage': (settings, uniqueId) {
      return PageRouteBuilder<dynamic>(
        ///透明弹窗页面这个需要是false
          opaque: false,
          ///背景蒙版颜色
          barrierColor: Colors.black12,
          settings: settings,
          pageBuilder: (_, __, ___) => DialogPage());
    },
  };

  static Route<dynamic> routeFactory(RouteSettings settings, String uniqueId) {
    FlutterBoostRouteFactory func = routerMap[settings.name];
    if (func == null) {
      if(kDebugMode) {
        func = routerMap['mainPage'];
      } else {
        return null;
      }
    }
    return func(settings, uniqueId);
  }
}