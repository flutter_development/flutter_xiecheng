
import 'package:flutter/material.dart';
import 'package:flutter_xiecheng/page/main/main_page.dart';

class JKRouter {
  // 首先展示的界面，第一个路由可以是 '/'
  static final String initialRoute = JKMainScreen.routeName;

  // 这里面的路由默认是弹出是系统的从右往左
  static final Map<String, WidgetBuilder> routes = {
    JKMainScreen.routeName: (ctx) => JKMainScreen(),
  };

  // 当routes里面找不到的时候，就会到这里根据名字找到对应的页面
  static final RouteFactory generateRoute = (settings) {
    return null;
  };

  // 根据路由找不到界面的时候做展示的错误页面
  static final RouteFactory unknownRoute = (settings) {
    return null;
  };
}