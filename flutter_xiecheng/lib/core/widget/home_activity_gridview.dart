import 'package:flutter/material.dart';
import 'package:flutter_boost/boost_navigator.dart';
import 'package:flutter_xiecheng/core/model/home_model.dart';

/// 首页顶部的占位条
class HomeActivityGridView extends StatefulWidget {
  // 数据
  final List<CommonModel> subNavList;
  const HomeActivityGridView({Key key, this.subNavList}) : super(key: key);

  @override
  State<HomeActivityGridView> createState() => _HomeActivityGridViewState();
}

class _HomeActivityGridViewState extends State<HomeActivityGridView> {
  ValueNotifier<bool> withContainer = ValueNotifier(false);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(6)
      ),
      child: Padding(
        padding: EdgeInsets.all(7),
        child: _items(context),
      ),
    );
  }

  Widget _items(BuildContext context) {
    if(widget.subNavList == null) return null;
    List<Widget> items = [];
    widget.subNavList.forEach((element) {
      items.add(_item(context, element));
    });

    // 计算一行展示的数量
    int separate = (widget.subNavList.length / 2 + 0.5).toInt();
    return Column(
      children: [
        Row(
          children: items.sublist(0, separate),
          mainAxisAlignment: MainAxisAlignment.spaceAround,
        ),
        Padding(
          padding: EdgeInsets.only(top: 10),
          child: Row(
            children: items.sublist(separate, items.length),
            mainAxisAlignment: MainAxisAlignment.spaceAround,
          ),
        )
      ],
    );
  }

  Widget _item(BuildContext context, CommonModel commodel) {
    return Expanded(
        flex: 1,
        child: _wrapGesture(Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.network(
              commodel.icon,
              width: 18,
              height: 18,
            ),
            Padding(
              padding: EdgeInsets.only(top: 3),
              child: Text(
                commodel.title,
                style: TextStyle(
                    fontSize: 12
                ),
              ),
            )
          ],
        ), commodel)
    );
  }

  Widget _wrapGesture(Widget widget, CommonModel commonModel) {
    return GestureDetector(
      onTap: () {
        setState(() {
          BoostNavigator.instance.push(
              "webview",
              withContainer: withContainer.value,
              arguments: {
                "hideAppBar": commonModel.hideAppBar,
                "icon": commonModel.icon,
                "statusBarColor": commonModel.statusBarColor,
                "title": commonModel.title,
                "url": commonModel.url,
              }
          );
        });
      },
      child: widget,
    );
  }
}
