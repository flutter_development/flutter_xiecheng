import 'package:flutter/material.dart';
import 'package:flutter_xiecheng/core/model/home_model.dart';

typedef TapCallBack = Function(CommonModel model);

/// 首页顶部的占位条
class HomeTopLocalGridView extends StatelessWidget {
  // 数据
  final List<CommonModel> models;
  final TapCallBack tapCallBack;
  const HomeTopLocalGridView({Key key, this.models, this.tapCallBack}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 64,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(7.0))
      ),
      child: Padding(
        padding: EdgeInsets.all(7),
        child: _items(context),
      ),
    );
  }

  Widget _items(BuildContext context) {
    if(models == null) return null;
    List<Widget> items = [];
    models.forEach((element) {
      items.add(_item(context, element));
    });
    return Row(
      children: items,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
    );
  }

  Widget _item(BuildContext context, CommonModel commodel) {
    return GestureDetector(
      onTap: () {
        tapCallBack(commodel);
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.network(
            commodel.icon,
            width: 32,
            height: 32,
          ),
          Text(
            commodel.title,
            style: TextStyle(
              fontSize: 12
            ),
          )
        ],
      ),
    );
  }
}
