import 'package:flutter/material.dart';
import 'package:flutter_boost/boost_navigator.dart';
import 'package:flutter_xiecheng/core/model/home_model.dart';

/// 首页最复杂的网格
class HomeBigGradView extends StatefulWidget {

  GridNavModel gridNavModel;

  HomeBigGradView({Key key, this.gridNavModel}) : super(key: key);

  @override
  State<HomeBigGradView> createState() => _HomeBigGradViewState();
}

class _HomeBigGradViewState extends State<HomeBigGradView> {

  ValueNotifier<bool> withContainer = ValueNotifier(false);

  @override
  Widget build(BuildContext context) {
    return PhysicalModel(
        color: Colors.transparent,
      borderRadius: BorderRadius.circular(6),
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: _gradViewItems(context),
      ),
    );
  }

  /// 创建子widget（大的整体）
  _gradViewItems(BuildContext context) {
    List<Widget> items = [];
    // 为空直接返回
    if(widget.gridNavModel == null) return items;
    if(widget.gridNavModel.hotel != null) {
      items.add(_gradViewItem(context, widget.gridNavModel.hotel, true));
    }
    if(widget.gridNavModel.flight != null) {
      items.add(_gradViewItem(context, widget.gridNavModel.flight, false));
    }
    if(widget.gridNavModel.travel != null) {
      items.add(_gradViewItem(context, widget.gridNavModel.travel, false));
    }
    return items;
  }

  /// 创建单个的一行：小的widget
  _gradViewItem(BuildContext context, GridNavItem itemModel, bool isFirst) {
    List<Widget> items = [];
    /// 先添加左边的大占位图
    items.add(_mainItem(context, itemModel.mainItem));
    items.add(_doubleItem(context, itemModel.item1, itemModel.item2));
    items.add(_doubleItem(context, itemModel.item3, itemModel.item4));

    List<Widget> expandItems = [];
    items.forEach((element) {
      expandItems.add(Expanded(child: element));
    });

    Color startColor = Color(int.parse('0xff' + itemModel.startColor));
    Color endColor = Color(int.parse('0xff' + itemModel.endColor));

    return Container(
      height: 88,
      margin: isFirst ? null : const EdgeInsets.only(top: 3),
      decoration: BoxDecoration(
        /// 线性渐变颜色
          gradient: LinearGradient(
              colors: [startColor, endColor]
          )
      ),
      child: Row(
        children: expandItems,
      ),
    );
  }

  /// 左边的大占位图
  Widget _mainItem(BuildContext context, CommonModel commonModel) {
    return _wrapGesture(
        Stack(
          alignment: AlignmentDirectional.topCenter,
          children: [
            Image.network(commonModel.icon, fit: BoxFit.contain, width: 121, height: 88, alignment: AlignmentDirectional.bottomEnd,),
            Container(
              margin: const EdgeInsets.only(top: 11),
              child: Text(commonModel.title, style: TextStyle(color: Colors.white, fontSize: 14),),
            )
          ],
        ),
        commonModel
    );
  }

  /// 叠加的两个小的item
  ///
  /// centerItem 是否值中间的item，中间的要设置分割线
  Widget _doubleItem(BuildContext context, CommonModel topModel, CommonModel bottomModel) {
    return Column(
      children: [
        Expanded(child: _singleItem(context, topModel, true)),
        Expanded(child: _singleItem(context, bottomModel, false)),
      ],
    );
  }

  /// 叠加的两个小的item
  ///
  /// firstItem：两个叠加的item，是否是上面的
  /// centerItem 整体2个是否值中间的item，中间的要设置分割线
  Widget _singleItem(BuildContext context, CommonModel model, bool firstItem) {
    /// 边框
    BorderSide borderSide = const BorderSide(width: 0.8, color: Colors.white);
    /// 水平方向撑满布局
    return FractionallySizedBox(
      widthFactor: 1,
      child: Container(
        decoration: BoxDecoration(
            border: Border(
                left: borderSide,
                bottom: firstItem ? borderSide : BorderSide.none
            )
        ),
        child: _wrapGesture(Center(
          child: Text(model.title, textAlign: TextAlign.center, style: TextStyle(fontSize: 14, color: Colors.white),),
        ), model),
      ),
    );
  }

  _wrapGesture(Widget widget, CommonModel commonModel) {
    return GestureDetector(
      onTap: () {
        setState(() {
          BoostNavigator.instance.push(
              "webview",
              withContainer: withContainer.value,
              arguments: {
                "hideAppBar": commonModel.hideAppBar,
                "icon": commonModel.icon,
                "statusBarColor": commonModel.statusBarColor,
                "title": commonModel.title,
                "url": commonModel.url,
              }
          );
        });
      },
      child: widget,
    );
  }
}
