import 'package:flutter/material.dart';
import 'package:flutter_boost/boost_navigator.dart';
import 'package:flutter_sizefit/flutter_sizefit.dart';
import 'package:flutter_xiecheng/core/model/home_model.dart';

/// 首页顶部的占位条
class HomeSalesBoxView extends StatefulWidget {
  // 数据
  final SalesBoxModel salesBoxModel;
  const HomeSalesBoxView({Key key, this.salesBoxModel}) : super(key: key);

  @override
  State<HomeSalesBoxView> createState() => _HomeSalesBoxViewState();
}

class _HomeSalesBoxViewState extends State<HomeSalesBoxView> {
  ValueNotifier<bool> withContainer = ValueNotifier(false);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: _items(context),
    );
  }

  Widget _items(BuildContext context) {
    if(widget.salesBoxModel == null) return null;
    List<Widget> items = [];
    items.add(_doubleItem(context, widget.salesBoxModel.bigCard1, widget.salesBoxModel.bigCard2, true, false));
    items.add(_doubleItem(context, widget.salesBoxModel.smallCard1, widget.salesBoxModel.smallCard2, false, false));
    items.add(_doubleItem(context, widget.salesBoxModel.smallCard3, widget.salesBoxModel.smallCard4, false, true));

    return Column(
      children: [
        Container(
          height: 44,
          margin: EdgeInsets.only(left: 10),
          decoration: BoxDecoration(
              border: Border(
                // 四个值 top right bottom left
                  bottom: BorderSide(
                    // 设置单侧边框的样式
                      color: Color(0xfff2f2f2),
                      width: 1,
                      style: BorderStyle.solid)
              )
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Image.network(widget.salesBoxModel.icon, height: 15, fit: BoxFit.fill,),
              Container(
                padding: EdgeInsets.fromLTRB(10, 1, 8, 1),
                margin: EdgeInsets.only(right: 7),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    gradient: LinearGradient(
                        colors: [Color(0xffff4e63), Color(0xffff6cc9)],
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight
                    )
                ),
                child: GestureDetector(
                  onTap: () {
                    BoostNavigator.instance.push(
                        "webview",
                        withContainer: withContainer.value,
                        arguments: {
                          "title": "更多活动",
                          "url": widget.salesBoxModel.moreUrl,
                        }
                    );
                  },
                  child: Text("获取更多福利 >", style: TextStyle(fontSize: 12, color: Colors.white),),
                ),
              )
            ],
          ),
        ),
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: items.sublist(0 ,1)
        ),
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: items.sublist(1 ,2)
        ),
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: items.sublist(2 ,3)
        )
      ],
    );
  }

  /// 顶部的大卡片
  ///
  /// isBigCard：是否是大卡片
  /// isLastCard：是否是最后一个卡片
  Widget _doubleItem(BuildContext context, CommonModel leftCardModel, CommonModel rightCardModel, bool isBigCard, bool isLastCard) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        _item(context, leftCardModel, isBigCard, true, isLastCard),
        _item(context, leftCardModel, isBigCard, false, isLastCard)
      ],
    );
  }

  Widget _item(BuildContext context, CommonModel commodel, bool big, bool isLeft, bool isLastCard) {
    /// 边框
    BorderSide borderSide = BorderSide(width: 0.8, color: Colors.white);
    return _wrapGesture(
        Container(
          // margin: isLastCard ? EdgeInsets.only(bottom: 0.8) : 0,
          decoration: BoxDecoration(
            border: Border(
                left: isLeft ? borderSide : BorderSide.none,
                bottom: isLastCard ?  BorderSide.none : borderSide
            ),
          ),
          child: Image.network(
            commodel.icon,
            width: JKSizeFit.screenWidth / 2 - 10,
            height: big ? 129 : 80,
          ),
        ), commodel);
  }

  Widget _wrapGesture(Widget widget, CommonModel commonModel) {
    return GestureDetector(
      onTap: () {
        setState(() {
          BoostNavigator.instance.push(
              "webview",
              withContainer: withContainer.value,
              arguments: {
                "hideAppBar": commonModel.hideAppBar,
                "icon": commonModel.icon,
                "statusBarColor": commonModel.statusBarColor,
                "title": commonModel.title,
                "url": commonModel.url,
              }
          );
        });
      },
      child: widget,
    );
  }
}
