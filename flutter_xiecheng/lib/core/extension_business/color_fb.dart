

import 'package:flutter/cupertino.dart';
import 'package:flutter_xiecheng/core/extension/color_extension.dart';

extension JKThemeColor on Color {
  // MARK: 品牌色
  /// B1-品牌橙色 #FF4600
  static Color cB1_Orange = HexStringColor.fromHex("#FF4600");
  /// B1-品牌银 #D8D8D8
  static Color cB1_Silver = HexStringColor.fromHex("#D8D8D8");
  // MARK: 辅助色
  /// A1 #4C4A57
  static Color cA1 = HexStringColor.fromHex("#4C4A57");
  /// A2 #C3A48F
  static Color cA2 = HexStringColor.fromHex("#C3A48F");
  /// A3 #FF9C31
  static Color cA3 = HexStringColor.fromHex("#FF9C31");
  /// A4 #FF4600
  static Color cA4 = HexStringColor.fromHex("#FF4600");
  /// A5 #00B49D
  static Color cA5 = HexStringColor.fromHex("#00B49D");
  /// A6 #F8E71C
  static Color cA6 = HexStringColor.fromHex("#F8E71C");
  /// A7 #447EFF
  static Color cA7 = HexStringColor.fromHex("#447EFF");
  /// A8 #3FC0FB
  static Color cA8 = HexStringColor.fromHex("#3FC0FB");
  /// A9 #FF7700
  static Color cA9 = HexStringColor.fromHex("#FF7700");
  /// A10 #8B40FF
  static Color cA10 = HexStringColor.fromHex("#8B40FF");

  //MARK:  数据图颜色
  static Color cB1_ETN = HexStringColor.fromHex("#0B48D0");
  static Color cB2 = HexStringColor.fromHex("#FF6B33");
  static Color cB3 = HexStringColor.fromHex("#FFBE99");
  static Color cB4 = HexStringColor.fromHex("#33C3B1");
  static Color cB5 = HexStringColor.fromHex("#FF6EE5");

// MARK: 中性色
  /// N1 适用于标题、文字主色、图标主色 #333333
  static Color cN1 = HexStringColor.fromHex("#333333");
  /// N2 适用辅助标题、辅助文字色、未选中、不可选置灰 #999999
  static Color cN2 = HexStringColor.fromHex("#999999");
  /// N3 适用于次级文字、或次级标题 #666666
  static Color cN3 = HexStringColor.fromHex("#666666");
  /// N4 适用于分割线 #EBEBEB
  static Color cN4 = HexStringColor.fromHex("#EBEBEB");
  /// N5 置灰态底色 #F2F3FA
  static Color cN5 = HexStringColor.fromHex("#F2F3FA");
  /// N6 灰色控件 #F7F7F8
  static Color cN6 = HexStringColor.fromHex("#F7F7F8");
  /// N7 背景底色 #FAFAFA
  static Color cN7 = HexStringColor.fromHex("#FAFAFA");
  /// N8 页面分割 #F2F2FA
  static Color cN8 = HexStringColor.fromHex("#F2F2FA");

  /// 涨 cA4
  static Color cStockRise = cA4;
  /// 停 cN2
  static Color cStockGray = cN2;
  /// 跌 c00B49D
  static Color cStockFall = HexStringColor.fromHex("#00B49D");
  /// 停 c1
  static Color cStockBlack = cN1;
  /// cEBEBEB
  static Color cCutline = cN4;

  /// #7C7C93
  static Color c7C7C93 = HexStringColor.fromHex("#7C7C93");
}


