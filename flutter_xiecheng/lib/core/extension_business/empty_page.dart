
import 'package:flutter/material.dart';
import 'package:flutter_sizefit/flutter_sizefit.dart';
import 'package:flutter_xiecheng/core/extension_business/color_fb.dart';
import 'package:flutter_xiecheng/gen_a/custom_asset.dart';

typedef EmptyRefreshBlock = Function(bool result);

/// 状态枚举
enum EmptyScreenStatus {
  // 加载成功但是数据为空
  empty,
  // 网络加载错误或者超时
  network_blocked,
}

class JKEmptyScreen extends StatefulWidget {
  /// 提示内容（上）
  final String tipContentTop;
  /// 提示图片
  final String tipImage;
  /// 状态
  final EmptyScreenStatus status;
  /// 刷新
  final EmptyRefreshBlock refresh;
  JKEmptyScreen({
    this.tipContentTop,
    this.tipImage,
    this.status = EmptyScreenStatus.empty,
    this.refresh
  });
  @override
  _JKEmptyScreenState createState() => _JKEmptyScreenState();
}

class _JKEmptyScreenState extends State<JKEmptyScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: SizedBox(),
            flex: 2,
          ),
          SizedBox(
            width: 100.0,
            height: 100.0,
            child: this.widget.tipImage == null ? getTipImage() : Image.asset(this.widget.tipImage),
          ),
          SizedBox(height: 27.px,),
          Text(
            this.widget.tipContentTop == null ? tipContent() : this.widget.tipContentTop,
            style: TextStyle(fontSize: 16.0, color: JKThemeColor.c7C7C93),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 15.px,),
          SizedBox(
            child: this.widget.status == EmptyScreenStatus.network_blocked ? TextButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(JKThemeColor.cB1_Orange),
                  //圆角
                  shape: MaterialStateProperty.all(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(17.px)
                      )
                  ),
                ),
                onPressed: () {
                  setState(() {
                    this.widget.refresh(true);
                  });
                },
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(refreshTittle(), style: TextStyle(
                        color: JKThemeColor.cN7,
                        fontSize: 16.px
                    ),),
                  ],
                )
            ) : null,
            width: 90.px,
            height: 34.px,
          ),
          Expanded(
            child: SizedBox(),
            flex: 3,
          ),
        ],
      ),
    );
  }

  /// 底部刷新按钮的文案
  String refreshTittle() {
    if(this.widget.status == EmptyScreenStatus.empty) {
      return "";
    } else if (this.widget.status == EmptyScreenStatus.network_blocked) {
      return "刷新";
    }
    return "";
  }

  /// 顶部提示的文案
  String tipContent() {
    if(this.widget.status == EmptyScreenStatus.empty) {
      return "暂无内容";
    } else if (this.widget.status == EmptyScreenStatus.network_blocked) {
      return "你的星球还在漂流\n请连接网络";
    }
    return "";
  }

  /// 顶部提示的文案
  Widget getTipImage() {
    if(this.widget.status == EmptyScreenStatus.empty) {
      return Image.asset(JKAssets.assets_images_empty_placeholder);
    } else if (this.widget.status == EmptyScreenStatus.network_blocked) {
      return Image.asset(JKAssets.assets_images_network_blocked);
    }
    return null;
  }
}
