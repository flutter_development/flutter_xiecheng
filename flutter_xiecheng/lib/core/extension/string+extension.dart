

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

/// 扩展String
extension StringExtension on String {

  ///自定义扩展方法
  log() {
    print('打印--$this');//用于打印对象是谁
  }

  /// 是否是电话号码
  bool get isMobileNumber {
    if(this?.isNotEmpty != true) return false;
    return RegExp(r'^((13[0-9])|(14[5,7,9])|(15[^4])|(18[0-9])|(17[0,1,3,5,6,7,8])|(19)[0-9])\d{8}$').hasMatch(this);
  }

  /// string 转 int
  int toInt() {
    return int.parse(this);
  }

  /// string 转 double
  double toDouble() {
    return double.parse(this);
  }

  /// string 转bool
  // (字符串 不等于'true' 不大于0 等于空时 返回false 否则返回true)
  bool toBool() {
    if (this.toLowerCase() == "true" ||
        this.toInt() > 0 ||
        this.toDouble() > 0 ||
        this.trim() != '' ||
        this != '') {
      return true;
    }
    return false;
  }

  /// 编码
  String toCoding() {
    String str = this.toString();
    // 对字符串进行编码
    return jsonEncode(Utf8Encoder().convert(str));
  }

  /// 解码
  String toDeCoding() {
    List<int> list = [];
    // 字符串解码
    jsonDecode(this).forEach(list.add);
    String value = Utf8Decoder().convert(list);
    return value;
  }
}

/// 提示框
extension ToastExtension on String {
  // 提示弹框
  toast({int timeInSecForIosWeb = 1}) {
    Fluttertoast.showToast(
        msg: this,
        // 时间长短Toast.LENGTH_SHORT(2500ms)、Toast.LENGTH_LONG（3500ms）
        toastLength: Toast.LENGTH_SHORT,
        // 消息框弹出的位置  （上中下）ToastGravity.TOP、ToastGravity.CENTER、 ToastGravity.BOTTOM
        gravity: ToastGravity.CENTER,
        // 消息框持续的时间（目前的版本只有ios有效）
        timeInSecForIosWeb: timeInSecForIosWeb,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0
    );
  }
}