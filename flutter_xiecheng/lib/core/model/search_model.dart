
/// 搜索的model
class SearchModel {
  List<SearchItem> data;
  String keyword;
  SearchModel({this.data, this.keyword});

  SearchModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data.add( SearchItem.fromJson(v));
      });
    }
    if (json['keyword'] != null) {
      keyword = json['keyword'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    if (this.keyword != null) {
      data['keyword'] = this.keyword;
    }
    return data;
  }
}

class SearchItem {
  String word;
  String type;
  String districtname;
  String url;

  SearchItem({this.word, this.type, this.districtname, this.url});

  SearchItem.fromJson(Map<String, dynamic> json) {
    word = json['word'];
    type = json['type'];
    districtname = json['districtname'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['word'] = this.word;
    data['type'] = this.type;
    data['districtname'] = this.districtname;
    data['url'] = this.url;
    return data;
  }
}

