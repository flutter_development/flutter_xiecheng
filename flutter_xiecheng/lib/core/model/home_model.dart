
/// 首页大接口的model
class HomeModel {
  List<BannerList> bannerList;
  ConfigModel config;
  GridNavModel gridNav;
  List<CommonModel> localNavList;
  SalesBoxModel salesBox;
  List<CommonModel> subNavList;

  HomeModel(
      {this.bannerList,
        this.config,
        this.gridNav,
        this.localNavList,
        this.salesBox,
        this.subNavList});

  HomeModel.fromJson(Map<String, dynamic> json) {
    if (json['bannerList'] != null) {
      bannerList = [];
      json['bannerList'].forEach((v) {
        bannerList.add(BannerList.fromJson(v));
      });
    }
    config =
    json['config'] != null ? ConfigModel.fromJson(json['config']) : null;
    gridNav =
    json['gridNav'] != null ? GridNavModel.fromJson(json['gridNav']) : null;
    if (json['localNavList'] != null) {
      localNavList = [];
      json['localNavList'].forEach((v) {
        localNavList.add(CommonModel.fromJson(v));
      });
    }
    salesBox = json['salesBox'] != null
        ? SalesBoxModel.fromJson(json['salesBox'])
        : null;
    if (json['subNavList'] != null) {
      subNavList = [];
      json['subNavList'].forEach((v) {
        subNavList.add(CommonModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    if (bannerList != null) {
      data['bannerList'] = bannerList.map((v) => v.toJson()).toList();
    }
    if (config != null) {
      data['config'] = config.toJson();
    }
    if (gridNav != null) {
      data['gridNav'] = gridNav.toJson();
    }
    if (localNavList != null) {
      data['localNavList'] = localNavList.map((v) => v.toJson()).toList();
    }
    if (salesBox != null) {
      data['salesBox'] = salesBox.toJson();
    }
    if (subNavList != null) {
      data['subNavList'] = subNavList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class BannerList {
  String icon;
  String url;

  BannerList({this.icon, this.url});

  BannerList.fromJson(Map<String, dynamic> json) {
    icon = json['icon'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['icon'] = icon;
    data['url'] = url;
    return data;
  }
}

class ConfigModel {
  String searchUrl;

  ConfigModel({this.searchUrl});

  ConfigModel.fromJson(Map<String, dynamic> json) {
    searchUrl = json['searchUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['searchUrl'] = searchUrl;
    return data;
  }
}

class GridNavModel {
  GridNavItem flight;
  GridNavItem hotel;
  GridNavItem travel;

  GridNavModel({this.flight, this.hotel, this.travel});

  GridNavModel.fromJson(Map<String, dynamic> json) {
    flight =
    json['flight'] != null ? GridNavItem.fromJson(json['flight']) : null;
    hotel = json['hotel'] != null ? GridNavItem.fromJson(json['hotel']) : null;
    travel =
    json['travel'] != null ? GridNavItem.fromJson(json['travel']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    if (flight != null) {
      data['flight'] = flight.toJson();
    }
    if (hotel != null) {
      data['hotel'] = hotel.toJson();
    }
    if (travel != null) {
      data['travel'] = travel.toJson();
    }
    return data;
  }
}

class GridNavItem {
  String endColor;
  CommonModel item1;
  CommonModel item2;
  CommonModel item3;
  CommonModel item4;
  CommonModel mainItem;
  String startColor;

  GridNavItem(
      {this.endColor,
        this.item1,
        this.item2,
        this.item3,
        this.item4,
        this.mainItem,
        this.startColor});

  GridNavItem.fromJson(Map<String, dynamic> json) {
    endColor = json['endColor'];
    item1 = json['item1'] != null ? CommonModel.fromJson(json['item1']) : null;
    item2 = json['item2'] != null ? CommonModel.fromJson(json['item2']) : null;
    item3 = json['item3'] != null ? CommonModel.fromJson(json['item3']) : null;
    item4 = json['item4'] != null ? CommonModel.fromJson(json['item4']) : null;
    mainItem = json['mainItem'] != null
        ? CommonModel.fromJson(json['mainItem'])
        : null;
    startColor = json['startColor'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['endColor'] = endColor;
    if (item1 != null) {
      data['item1'] = item1.toJson();
    }
    if (item2 != null) {
      data['item2'] = item2.toJson();
    }
    if (item3 != null) {
      data['item3'] = item3.toJson();
    }
    if (item4 != null) {
      data['item4'] = item4.toJson();
    }
    if (mainItem != null) {
      data['mainItem'] = mainItem.toJson();
    }
    data['startColor'] = startColor;
    return data;
  }
}

class CommonModel {
  bool hideAppBar;
  String icon;
  String statusBarColor;
  String title;
  String url;

  CommonModel(
      {this.hideAppBar, this.icon, this.statusBarColor, this.title, this.url});

  CommonModel.fromJson(Map<String, dynamic> json) {

    if(json['hideAppBar'] != null) {
      hideAppBar = json['hideAppBar'];
    }
    if(json['icon'] != null) {
      icon = json['icon'];
    }
    if(json['statusBarColor'] != null) {
      statusBarColor = json['statusBarColor'];
    }
    if(json['title'] != null) {
      title = json['title'];
    }
    if(json['url'] != null) {
      url = json['url'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['hideAppBar'] = hideAppBar;
    data['icon'] = icon;
    data['statusBarColor'] = statusBarColor;
    data['title'] = title;
    data['url'] = url;
    return data;
  }
}

class SalesBoxModel {
  CommonModel bigCard1;
  CommonModel bigCard2;
  String icon;
  String moreUrl;
  CommonModel smallCard1;
  CommonModel smallCard2;
  CommonModel smallCard3;
  CommonModel smallCard4;

  SalesBoxModel(
      {this.bigCard1,
        this.bigCard2,
        this.icon,
        this.moreUrl,
        this.smallCard1,
        this.smallCard2,
        this.smallCard3,
        this.smallCard4});

  SalesBoxModel.fromJson(Map<String, dynamic> json) {
    bigCard1 = json['bigCard1'] != null
        ? CommonModel.fromJson(json['bigCard1'])
        : null;
    bigCard2 = json['bigCard2'] != null
        ? CommonModel.fromJson(json['bigCard2'])
        : null;
    icon = json['icon'];
    moreUrl = json['moreUrl'];
    smallCard1 = json['smallCard1'] != null
        ? CommonModel.fromJson(json['smallCard1'])
        : null;
    smallCard2 = json['smallCard2'] != null
        ? CommonModel.fromJson(json['smallCard2'])
        : null;
    smallCard3 = json['smallCard3'] != null
        ? CommonModel.fromJson(json['smallCard3'])
        : null;
    smallCard4 = json['smallCard4'] != null
        ? CommonModel.fromJson(json['smallCard4'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    if (bigCard1 != null) {
      data['bigCard1'] = bigCard1.toJson();
    }
    if (bigCard2 != null) {
      data['bigCard2'] = bigCard2.toJson();
    }
    data['icon'] = icon;
    data['moreUrl'] = moreUrl;
    if (smallCard1 != null) {
      data['smallCard1'] = smallCard1.toJson();
    }
    if (smallCard2 != null) {
      data['smallCard2'] = smallCard2.toJson();
    }
    if (smallCard3 != null) {
      data['smallCard3'] = smallCard3.toJson();
    }
    if (smallCard4 != null) {
      data['smallCard4'] = smallCard4.toJson();
    }
    return data;
  }
}