
// 热榜列表
class JKHotListModel {
  String id;
  String source;
  String publishTime;
  String publishTimeString;
  String title;
  String imageUrl;
  String topFlag;
  String articleUrl;

  JKHotListModel(
      {this.id,
        this.source,
        this.publishTime,
        this.publishTimeString,
        this.title,
        this.imageUrl,
        this.topFlag,
        this.articleUrl});

  JKHotListModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    source = json['source'];
    publishTime = json['publishTime'];
    publishTimeString = json['publishTimeString'];
    title = json['title'];
    imageUrl = json['imageUrl'];
    topFlag = json['topFlag'];
    articleUrl = json['articleUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['source'] = this.source;
    data['publishTime'] = this.publishTime;
    data['publishTimeString'] = this.publishTimeString;
    data['title'] = this.title;
    data['imageUrl'] = this.imageUrl;
    data['topFlag'] = this.topFlag;
    data['articleUrl'] = this.articleUrl;
    return data;
  }
}

