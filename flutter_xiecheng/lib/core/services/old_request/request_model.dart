import 'package:flutter_xiecheng/core/model/hot_list_model.dart';
import 'package:flutter_xiecheng/core/services/http_base_model.dart';
import 'package:flutter_xiecheng/core/services/http_new_request.dart';
import 'package:flutter_xiecheng/core/services/old_request/http_request.dart';
import 'package:flutter_xiecheng/core/services/urls.dart';

typedef RequestBlock = Function<T>(bool isSuccess, String errorMesage, T model);

class JKRequestModel {

  // 1、资讯热榜(Future的形式)
  static Future<Map> getHotList() async {
    // 网络请求
    final url = kHotListUrl;
    final result = await HttpRequest.post(url);
    // json转model
    final hotlist = result['data'];
    List<JKHotListModel> hotlists = [];
    for (var json in hotlist) {
      hotlists.add(JKHotListModel.fromJson(json));
    }
    var isSuccess = "1";
    var errorMessage = "";
    if (result['resultCode'] != "1") {
       isSuccess = "0";
       errorMessage = result['msg'];
    }
    return {"models": hotlists, "isSuccess": isSuccess, "errorMessage": errorMessage};
  }

  // 2、资讯热榜(闭包的形式)
  static getHotList2(HttpSuccessCallBack success,
      HttpFailCallBack error) async {
    // 网络请求
    final url = kHotListUrl;
    await JKHttpRequest.post<HttpResponseListEntity, JKHotListModel>(url, success: success, error: error);
  }
}


   // 旧的使用
   /*
    JKRequestModel.getHotList().then((result) {
      print("火箭---$result---------🚀🚀🚀🚀🚀🚀🚀🚀");
      // 结束刷新
      _controller.finishRefresh();
      setState(() {
        if (result["isSuccess"] == "1") {
          _hotListModels = [];
          // 请求成功
          _hotListModels = result["models"];

        } else {
          // 请求失败
          print("请求失败💣💣💣💣💣💣💣");
        }
      });
    });
    */