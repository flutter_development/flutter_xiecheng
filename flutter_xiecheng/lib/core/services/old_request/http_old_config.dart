

import 'package:flutter/foundation.dart';

class HttpOldConfig {
  // 超时时间
  static const int timeout = 5000;
  // 正式：url
  static const String baseFormalUrl = 'https://m.ctrip.com';
  // 测试：url
  static const String baseTestUrl = 'https://m.ctrip.com';
  // apizza：url
  static const String baseApizzaUrl = 'https://m.ctrip.com';
  // 根据环境获取不同的基础url
  static String get baseUrl {
    if (kReleaseMode){
      // release
      return baseFormalUrl;
    }else {
      // debug
      return baseTestUrl;
    }
  }
}