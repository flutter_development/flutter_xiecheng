//
// import 'dart:convert';
//
// import 'package:favorcate/core/model/category_model.dart';
// import 'package:flutter/services.dart';
//
// // 这里展示的 json 文件的加载过程
// class JKJsonParse {
//   static Future<List<JKCategoryModel>> getCategoryData() async {
//     // 1.加载json文件
//     final jsonString = await rootBundle.loadString("assets/json/category.json");
//
//     // 2.将jsonString转成Map/List
//     final result =  json.decode(jsonString);
//
//     // 3.将Map中的内容转成一个个对象
//     final resultList = result["category"];
//     List<JKCategoryModel> categories = [];
//     for (var json in resultList) {
//       categories.add(JKCategoryModel.fromJson(json));
//     }
//     return categories;
//   }
// }