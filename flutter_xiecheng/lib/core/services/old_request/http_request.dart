
import 'package:dio/dio.dart';
import 'package:flutter_xiecheng/core/services/http_config.dart';
import 'package:flutter_xiecheng/core/services/old_request/http_old_config.dart';

// 旧的：暂时不用
class HttpRequest {

  // BaseOptions、Options、RequestOptions 都可以配置参数，优先级别依次递增，且可以根据优先级别覆盖参数
  static final BaseOptions baseOptions = BaseOptions(
    // //请求基地址,可以包含子路径
    baseUrl: HttpOldConfig.baseUrl,
    // 务器超时时间，单位是毫秒.
    connectTimeout: HttpOldConfig.timeout,
    // 响应流上前后两次接受到数据的间隔，单位为毫秒。
    receiveTimeout: 5000,
    headers: {
      //do something
      "version": "1.0.0"
    },
    //请求的Content-Type，默认值是"application/json; charset=utf-8",Headers.formUrlEncodedContentType会自动编码请求体.
    //contentType: Headers.formUrlEncodedContentType,
    //表示期望以那种格式(方式)接受响应数据。接受四种类型 `json`, `stream`, `plain`, `bytes`. 默认值是 `json`,
    //responseType: ResponseType.plain,
  );
  ///
  CancelToken cancelToken = CancelToken();
  /// Dio 对象
  static final Dio dio = Dio(baseOptions);

  static Future<T> _request<T>(String url, {
    String method = 'get',
    Map<String, dynamic> params, Interceptor inter}) async {
    // 1、添加日志请求拦截 显示日志
    // dio.interceptors.add(LogInterceptor(responseBody: true,requestBody: true)); //开启请求日志

    // 2、Cookie管理 这个暂时不清楚
    //dio.interceptors.add(CookieManager(CookieJar()));

    print("请求连接：${HttpConfig.baseUrl}${url} 请求类型：$method");

    // 3.创建单独配置
    final options = Options(method: method);
    // 全局拦截器
    // 创建默认的全局拦截器
    InterceptorsWrapper dInter = InterceptorsWrapper(
        onRequest:(options, handler){
          print('请求拦截');
          // Do something before request is sent
          return handler.next(options); //continue
          // 如果你想完成请求并返回一些自定义数据，你可以resolve一个Response对象 `handler.resolve(response)`。
          // 这样请求将会被终止，上层then会被调用，then中返回的数据将是你的自定义response.
          //
          // 如果你想终止请求并触发一个错误,你可以返回一个`DioError`对象,如`handler.reject(error)`，
          // 这样请求将被中止并触发异常，上层catchError会被调用。
        },
        onResponse:(response,handler) {
          print('响应拦截');
          // Do something with response data
          return handler.next(response); // continue
          // 如果你想终止请求并触发一个错误,你可以 reject 一个`DioError`对象,如`handler.reject(error)`，
          // 这样请求将被中止并触发异常，上层catchError会被调用。
        },
        onError: (DioError e, handler) {
          print('错误拦截');
          // Do something with response error
          return handler.next(e);//continue
          // 如果你想完成请求并返回一些自定义数据，可以resolve 一个`Response`,如`handler.resolve(response)`。
          // 这样请求将会被终止，上层then会被调用，then中返回的数据将是你的自定义response.
        }
    );

    List<Interceptor> inters = [dInter];
    if (inter != null) {
      inters.add(inter);
    }
    // 统一添加到拦截器中
    dio.interceptors.addAll(inters);

    // 4、发送网络请求
    try {
      Response response = await dio.request(
          url, queryParameters: params, options: options);
      return response.data;
    } on DioError catch (e) {
      return Future.error(e);
    }
  }

  // error统一处理
  void formatError(DioError e) {
    if (e.type == DioErrorType.connectTimeout) {
      // It occurs when url is opened timeout.
      print("连接超时");
    } else if (e.type == DioErrorType.sendTimeout) {
      // It occurs when url is sent timeout.
      print("请求超时");
    } else if (e.type == DioErrorType.receiveTimeout) {
      //It occurs when receiving timeout
      print("响应超时");
    } else if (e.type == DioErrorType.response) {
      // When the server response, but with a incorrect status, such as 404, 503...
      print("出现异常");
    } else if (e.type == DioErrorType.cancel) {
      // When the request is cancelled, dio will throw a error with this type.
      print("请求取消");
    } else {
      //DEFAULT Default error type, Some other Error. In this case, you can read the DioError.error if it is not null.
      print("未知错误");
    }
  }

  /*
   * 取消请求
   *
   * 同一个cancel token 可以用于多个请求，当一个cancel token取消时，所有使用该cancel token的请求都会被取消。
   * 所以参数可选
   */
  void cancelRequests(CancelToken token) {
    token.cancel("cancelled");
  }

  // get网络请求
  static Future<T> get<T>(String url, {
    Map<String, dynamic> params, Interceptor inter}) async {
    return await _request(url, method: 'get', params: params, inter: inter);
  }

  // post网络请求
  static Future<T> post<T>(String url, {
    Map<String, dynamic> params, Interceptor inter}) async {
    return await _request(url, method: 'post', params: params, inter: inter);
  }

}