import 'package:flutter_xiecheng/core/model/home_model.dart';

class HttpResponseEntity<T> {
  T data;
  String msg;
  String errorCode;
  String resultCode;

  HttpResponseEntity({this.resultCode, this.data, this.msg, this.errorCode});

  HttpResponseEntity.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? EntityFactory.generateOBJ<T>(json['data']) : null;
    msg = json['msg'];
    errorCode = json['errorCode'];
    errorCode = json['errorCode'];
  }
}

class HttpResponseListEntity<T> {
  List<T> data;
  String msg;
  String errorCode;
  String resultCode;

  HttpResponseListEntity({this.resultCode, this.data, this.msg, this.errorCode});

  HttpResponseListEntity.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = [];
      (json['data'] as List).forEach((v) {
        data.add(EntityFactory.generateOBJ<T>(v));
      });
    }
    msg = json['msg'];
    errorCode = json['errorCode'];
    errorCode = json['errorCode'];
  }
}

/// 所有的大model都需要在此添加
class EntityFactory {
  static T generateOBJ<T>(json) {
    if (T.toString() == "HomeModel") {
      return HomeModel.fromJson(json) as T;
    } else {
      return null;
    }
  }
}