
import 'package:dio/dio.dart';
import 'package:flutter_xiecheng/core/extension_business/custom_print.dart';
import 'package:flutter_xiecheng/core/services/http_base_model.dart';
import 'package:flutter_xiecheng/core/services/http_config.dart';

typedef HttpSuccessCallBack<P> = dynamic Function(P response);
typedef HttpFailCallBack = Function(ErrorResponse errorMessage);

/// 请求错误
class ErrorResponse {
  DioErrorType errorType;
  String errorTypeContent;
  ErrorResponse(this.errorType, this.errorTypeContent);
}

class JKHttpRequest {

  // BaseOptions、Options、RequestOptions 都可以配置参数，优先级别依次递增，且可以根据优先级别覆盖参数
  static final BaseOptions baseOptions = BaseOptions(
    // //请求基地址,可以包含子路径
    baseUrl: HttpConfig.baseUrl,
    // 务器超时时间，单位是毫秒.
    connectTimeout: HttpConfig.timeout,
    // 响应流上前后两次接受到数据的间隔，单位为毫秒。
    receiveTimeout: 5000,
    headers: {
      //do something
      "version": "1.0.0"
    },
    //请求的Content-Type，默认值是"application/json; charset=utf-8",Headers.formUrlEncodedContentType会自动编码请求体.
    //contentType: Headers.formUrlEncodedContentType,
    //表示期望以那种格式(方式)接受响应数据。接受四种类型 `json`, `stream`, `plain`, `bytes`. 默认值是 `json`,
    //responseType: ResponseType.plain,
  );
  ///
  CancelToken cancelToken = CancelToken();
  /// Dio 对象
  static final Dio dio = Dio(baseOptions);

  static _request<T, G>(String url, {
    String method = 'get',
    Map<String, dynamic> params, Interceptor inter,
    HttpSuccessCallBack success,
    HttpFailCallBack error}) async {
    // 1、添加日志请求拦截 显示日志
    // dio.interceptors.add(LogInterceptor(responseBody: true,requestBody: true)); //开启请求日志

    // 2、Cookie管理 这个暂时不清楚
    //dio.interceptors.add(CookieManager(CookieJar()));
    // myPrint("请求连接：${HttpConfig.baseUrl}${url} 请求类型：$method");

    // 3.创建单独配置
    final options = Options(method: method);
    // 全局拦截器
    // 创建默认的全局拦截器
    InterceptorsWrapper dInter = InterceptorsWrapper(
        onRequest:(options, handler){
          myPrint(
              "\n================================= 请求数据 =================================", StackTrace.current);
          myPrint("method = ${options.method.toString()}", StackTrace.current);
          myPrint("url = ${options.uri.toString()}", StackTrace.current);
          myPrint("headers = ${options.headers}", StackTrace.current);
          myPrint("params = ${options.queryParameters}", StackTrace.current);
          // Do something before request is sent
          return handler.next(options); //continue
          // 如果你想完成请求并返回一些自定义数据，你可以resolve一个Response对象 `handler.resolve(response)`。
          // 这样请求将会被终止，上层then会被调用，then中返回的数据将是你的自定义response.
          //
          // 如果你想终止请求并触发一个错误,你可以返回一个`DioError`对象,如`handler.reject(error)`，
          // 这样请求将被中止并触发异常，上层catchError会被调用。
        },
        onResponse:(response,handler) {
          myPrint(
              "\n================================= 响应数据开始 =================================", StackTrace.current);
          myPrint("code = ${response.statusCode}", StackTrace.current);
          myPrint("data = ${response.data}", StackTrace.current);
          myPrint(
              "================================= 响应数据结束 =================================\n", StackTrace.current);
          return handler.next(response); // continue
          // 如果你想终止请求并触发一个错误,你可以 reject 一个`DioError`对象,如`handler.reject(error)`，
          // 这样请求将被中止并触发异常，上层catchError会被调用。
        },
        onError: (DioError e, handler) {
          myPrint(
              "\n=================================错误响应数据 =================================", StackTrace.current);
          myPrint("type = ${e.type}", StackTrace.current);
          myPrint("message = ${e.message}", StackTrace.current);
          myPrint("stackTrace = ${e.stackTrace}", StackTrace.current);
          myPrint("\n", StackTrace.current);
          // Do something with response error
          return handler.next(e);//continue
          // 如果你想完成请求并返回一些自定义数据，可以resolve 一个`Response`,如`handler.resolve(response)`。
          // 这样请求将会被终止，上层then会被调用，then中返回的数据将是你的自定义response.
        }
    );

    List<Interceptor> inters = [dInter];
    if (inter != null) {
      inters.add(inter);
    }
    // 统一添加到拦截器中
    dio.interceptors.addAll(inters);

    // 4、发送网络请求
    try {
      Response response = await dio.request(
          url, queryParameters: params, options: options);
      if (response.statusCode == 200) {
        if (response.data['resultCode'] == 1) {
          _httpSuccess<T, G>(response.data, success);
        } else {
          _httpSuccess<T, G>(response.data, success);
          // _httpSpecial(response.data, special);
        }
      } else {
        if (error != null) {
          error(ErrorResponse(DioErrorType.other, "网络状况不佳"));
        }
      }
    } on DioError catch (e) {
      _httpError(e, error);
    }
  }

  /// statusCode==200 && resultCode==1
  static _httpSuccess<T, G>(Map<String, dynamic> response, HttpSuccessCallBack success) {
    if (success != null) {
      if (T.toString() == "HttpResponseEntity<dynamic>") {
        success((HttpResponseEntity<G>.fromJson(response).data));
      } else if (T.toString() == "HttpResponseListEntity<dynamic>") {
        success(HttpResponseListEntity<G>.fromJson(response).data);
      } else {
        success(response['resultCode']);
      }
    }
  }

  /// statusCode==200 && resultCode != 1  暂时没处理
  static _httpSpecial(Map<String, dynamic> response, HttpSuccessCallBack special) {
    ///可以针对不同的Flag 进行处理同一处理
    if (special != null) {
      special(response);
    }
  }

  /// statusCode != 200
  static _httpError(DioError e, HttpFailCallBack error) {
    // print('httpError-----${{e.message}');
    if (error != null) {
      error(_formatError(e));
    }
  }

  // error统一处理
  static ErrorResponse _formatError(DioError e) {
    if (e.type == DioErrorType.connectTimeout) {
      // It occurs when url is opened timeout.
      myPrint("连接超时", StackTrace.current);
      return ErrorResponse(DioErrorType.connectTimeout, "连接超时");
    } else if (e.type == DioErrorType.sendTimeout) {
      // It occurs when url is sent timeout.
      myPrint("请求超时", StackTrace.current);
      return ErrorResponse(DioErrorType.sendTimeout, "请求超时");
    } else if (e.type == DioErrorType.receiveTimeout) {
      //It occurs when receiving timeout
      myPrint("响应超时", StackTrace.current);
      return ErrorResponse(DioErrorType.receiveTimeout, "响应超时");
    } else if (e.type == DioErrorType.response) {
      // When the server response, but with a incorrect status, such as 404, 503...
      myPrint("出现异常", StackTrace.current);
      return ErrorResponse(DioErrorType.response, "出现异常");
    } else if (e.type == DioErrorType.cancel) {
      // When the request is cancelled, dio will throw a error with this type.
      myPrint("请求取消", StackTrace.current);
      return ErrorResponse(DioErrorType.cancel, "请求取消");
    } else {
      //DEFAULT Default error type, Some other Error. In this case, you can read the DioError.error if it is not null.
      myPrint("网络状况不佳", StackTrace.current);
      return ErrorResponse(DioErrorType.other, "网络状况不佳");
    }
  }

  /*
   * 取消请求--- 暂时无用
   *
   * 同一个cancel token 可以用于多个请求，当一个cancel token取消时，所有使用该cancel token的请求都会被取消。
   * 所以参数可选
   */
  void cancelRequests(CancelToken token) {
    token.cancel("cancelled");
  }

  // get网络请求
  static get<T, G>(String url, {
    Map<String, dynamic> params, Interceptor inter, HttpSuccessCallBack success,
    HttpFailCallBack error}) async {
    return await _request<T, G>(url, method: 'get', params: params, inter: inter, success: success, error: error);
  }

  // post网络请求
  static post<T, G>(String url, {
    Map<String, dynamic> params, Interceptor inter, HttpSuccessCallBack success,
    HttpFailCallBack error}) async {
    return await _request<T, G>(url, method: 'post', params: params, inter: inter, success: success, error: error);
  }
}