
import 'package:flutter/foundation.dart';

class HttpConfig {
  // 超时时间
  static const int timeout = 5000;
  // 正式：url
  static const String baseFormalUrl = 'http://rest.apizza.net/mock/b0e6ce7c4d7857ba71c5adb8ad2b942f';
  // 测试：url
  static const String baseTestUrl = 'http://rest.apizza.net/mock/b0e6ce7c4d7857ba71c5adb8ad2b942f';
  // apizza：url
  static const String baseApizzaUrl = 'http://rest.apizza.net/mock/b0e6ce7c4d7857ba71c5adb8ad2b942f';
  // 根据环境获取不同的基础url
  static String get baseUrl {
    if (kReleaseMode){
      // release
      return baseFormalUrl;
    }else {
      // debug
      return baseTestUrl;
    }
  }
}