
import 'package:flutter/material.dart';

class JKLoadingWidget extends StatelessWidget {
  @required Widget child;
  @required bool isShow;
  String text;
  List<Widget> children = [];

  JKLoadingWidget({ Key key, this.child, this.isShow, this.text})
      : assert(child != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    children.add(child);
    if (isShow) {
      children.add(SizedBox.expand( //
          child: Container(
            decoration: ShapeDecoration(
              color: Colors.black54,
              shape: RoundedRectangleBorder(),
            ),
            child: new Center( //保证控件居中效果
              child: new SizedBox(
                width: 120.0,
                height: 120.0,
                child: Container(
                  decoration: ShapeDecoration(
                    color: Color(0xffffffff),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(8.0),
                      ),
                    ),
                  ),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      CircularProgressIndicator(),
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 20.0,
                        ),
                        child: Text(
                          text,
                          style: TextStyle(fontSize: 12.0),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )// 创建透明层
      ));
    }
    return Stack(children: children);
  }
}

class JKTestLoadingContainer extends StatelessWidget {
  final Widget child;
  final bool isLoading;
  /// 是否覆盖child
  final bool cover;

  const JKTestLoadingContainer({Key key, @required this.isLoading, this.cover = false, @required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (!cover && !isLoading) return _loadingView;
    return Stack(
    children: [
      child, isLoading ? _loadingView : null
    ],
    );
  }

  Widget get _loadingView {
    return Center(
      child: CircularProgressIndicator(),
    );
  }
}
