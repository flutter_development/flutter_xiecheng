
import 'package:flutter/material.dart';

class ImagesAnim extends StatefulWidget {
  final List<String> imageCaches;
  final double width;
  final double height;
  final Color backColor;

  ImagesAnim(this.imageCaches, this.width, this.height, this.backColor, {Key key})
      : assert(imageCaches != null),
        super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new _WOActionImageState();
  }
}

class _WOActionImageState extends State<ImagesAnim> {
  bool _disposed;
  Duration _duration;
  int _imageIndex;
  Container _container;

  @override
  void initState() {
    super.initState();
    _disposed = false;
    _duration = Duration(milliseconds: 80);
    _imageIndex = 0;
    _container = Container(
      height: widget.height,
      width: widget.width,
      decoration: BoxDecoration(
          color: Colors.red,
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(28)
      ),
    );
    _updateImage();
  }

  void _updateImage() {
    if (_disposed || widget.imageCaches.isEmpty) {
      return;
    }

    setState(() {
      print("🚀-----$_imageIndex --${widget.imageCaches[_imageIndex]}");
      if (_imageIndex > widget.imageCaches.length - 2) {
        _imageIndex = 0;
      }
      _container = Container(
          color: widget.backColor,
          child: Image.asset(widget.imageCaches[_imageIndex]),
          height: widget.height,
          width: widget.width
      );
      _imageIndex++;
    });
    Future.delayed(_duration, () {
      _updateImage();
    });
  }

  @override
  void dispose() {
    super.dispose();
    _disposed = true;
    widget.imageCaches.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child:  _container,
    );
    //   Container(
    //   decoration: BoxDecoration(
    //     color: Colors.green,
    //     borderRadius: BorderRadius.all(
    //       Radius.circular(10.0),
    //     ),
    //   ),
    //   height: widget.height + 20,
    //   width: widget.width + 20,
    //   child:,
    // );
  }
}