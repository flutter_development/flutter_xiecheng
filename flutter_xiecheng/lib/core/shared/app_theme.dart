import 'package:flutter/material.dart';
class JKAppTheme {

  /* 1、共有的属性 */
  // 字体的设置
  static const double bodyText2FontSize = 8.0;
  static const double smallFontSize = 12.0;
  static const double normalFontSize = 16.0;
  static const double largeFontSize = 22.0;

  // 颜色的设置
  // 正常的颜色
  static final Color normalTextColor = Colors.black87;
  // 暗黑模式的字体颜色
  static final Color darkTextColor = Colors.yellow;

  /* 2、正常的模式 */
  static final ThemeData normalTheme = ThemeData(
    // 导航nav 底部bar 等颜色
      primarySwatch: Colors.pink,
      // 给每个页面增加一个背景色
      canvasColor: Color.fromRGBO(255, 254, 222, 1),
      // 点击的水波纹设置为无色
      highlightColor: Colors.transparent,
      textTheme: TextTheme(
        bodyText2: TextStyle(fontSize: bodyText2FontSize, color: normalTextColor),
        headline5: TextStyle(fontSize: smallFontSize, color: normalTextColor),
        headline4: TextStyle(fontSize: normalFontSize, color: normalTextColor),
        headline3: TextStyle(fontSize: largeFontSize, color: normalTextColor),
      )
  );

  /* 3、暗黑模式 */
  static final ThemeData darkTheme = ThemeData(
      primaryColor: Colors.yellow,
      // 点击的水波纹设置为无色
      highlightColor: Colors.transparent,
      textTheme: TextTheme(
          subtitle1: TextStyle(fontSize: normalFontSize, color: darkTextColor)
      )
  );
}