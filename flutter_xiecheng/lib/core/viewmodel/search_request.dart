

import 'package:flutter_xiecheng/core/model/search_model.dart';
import 'package:flutter_xiecheng/core/services/old_request/http_request.dart';
import 'package:flutter_xiecheng/core/services/urls.dart';

class JKSearchRequest {

  // 1、搜索(Future的形式)
  static Future<SearchModel> getSearch(String keyword) async {
    // 网络请求
    final url = kSearchUrl;
    final resultJson = await HttpRequest.get(url + '&keyword=' + keyword);
    // print("🚀：$resultJson");
    // json转model
    SearchModel searchModel = SearchModel.fromJson(resultJson);
    /// 只有当前输入的内容和服务器返回的内容一直时才渲染
    searchModel.keyword = keyword;
    return searchModel;
  }
}