
import 'package:flutter_xiecheng/core/model/home_model.dart';
import 'package:flutter_xiecheng/core/services/http_base_model.dart';
import 'package:flutter_xiecheng/core/services/http_new_request.dart';
import 'package:flutter_xiecheng/core/services/urls.dart';

class JKHomeRequest {
  // 1、首页接口
  static getHomeData(HttpSuccessCallBack success,
      HttpFailCallBack error) async {
    // 网络请求
    final url = kHomeUrl;
    await JKHttpRequest.get<HttpResponseEntity, HomeModel>(url, success: success, error: error);
  }
}