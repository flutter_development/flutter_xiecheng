
import 'package:flutter/material.dart';
import 'package:flutter_sizefit/flutter_sizefit.dart';
import 'package:flutter_xiecheng/core/tools/imags_animation.dart';
import 'package:flutter_xiecheng/gen_a/custom_asset.dart';

class SettingScreen extends StatelessWidget {
  ValueNotifier<bool> withContainer = ValueNotifier(false);
  @override
  Widget build(BuildContext context) {
    List<String> images = [JKAssets.assets_images_flutter_loading_1,
      JKAssets.assets_images_flutter_loading_2,
      JKAssets.assets_images_flutter_loading_3,
      JKAssets.assets_images_flutter_loading_4,
      JKAssets.assets_images_flutter_loading_5,
      JKAssets.assets_images_flutter_loading_6,
      JKAssets.assets_images_flutter_loading_7,
      JKAssets.assets_images_flutter_loading_8,
      JKAssets.assets_images_flutter_loading_9,
      JKAssets.assets_images_flutter_loading_10,
      JKAssets.assets_images_flutter_loading_11,
      JKAssets.assets_images_flutter_loading_12];

    return Scaffold(
      appBar: AppBar(
        title: Text('暂时无用的主界面'),
      ),
      body: Container(
          width: JKSizeFit.screenWidth,
          decoration: BoxDecoration(
              color: Colors.white
          ),
          child: Center(
            child:ImagesAnim(images, 60.px, 60.px, Colors.brown),
          ),
      ),
    );;
  }
}
