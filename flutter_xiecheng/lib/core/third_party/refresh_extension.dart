import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';

class Application{
  static Header getHeader({Color textColor = Colors.black87, Color infoColor = Colors.black87}) {
    return ClassicalHeader(
      refreshText: '下拉刷新',
      refreshReadyText: '准备刷新',
      refreshingText: '刷新中...',
      refreshedText: '刷新完成',
      infoText: '更新于 %T',
      bgColor: Colors.transparent,
      textColor: textColor,
      infoColor: infoColor,
      showInfo: true,
    );
  }

  static Footer getFooter() {
    return ClassicalFooter(
      loadText: '上拉加载',
      loadReadyText: '准备加载',
      loadingText: '加载中...',
      loadedText: '加载完成',
      noMoreText: '没有更多',
      infoText: '更新于 %T',
      bgColor: Colors.transparent,
      textColor: Colors.black87,
      infoColor: Colors.black54,
      showInfo: true,
    );
  }
}
