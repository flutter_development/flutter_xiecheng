
import 'package:flutter/material.dart';
import 'package:flutter_xiecheng/page/main/initalize_items.dart';

class JKMainScreen extends StatefulWidget {
  // 默认路由写个 / 就好
  static const String routeName = '/';
  final String data;

  JKMainScreen({this.data});

  @override
  _JKMainScreenState createState() => _JKMainScreenState();
}

class _JKMainScreenState extends State<JKMainScreen> {
  // 选中的页码
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _currentIndex,
        children: pages,
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        // 选中的字体大小
        selectedFontSize: 12,
        // 未选中的字体大小
        unselectedFontSize: 12,
        // 选中的字体颜色
        selectedItemColor: Colors.blue,
        // 未选中的字体颜色
        unselectedItemColor: Colors.grey,
        // 超过四个的时候必须设置它，不然多出的就会被隐藏掉
        type: BottomNavigationBarType.fixed,
        items: items,
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
      ),
      // drawer: JKHomeDrawer(),
    );
  }
}
