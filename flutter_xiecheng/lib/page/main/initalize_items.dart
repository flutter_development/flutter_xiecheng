

import 'package:flutter/material.dart';
import 'package:flutter_xiecheng/page/home/home_page.dart';
import 'package:flutter_xiecheng/page/main/bottom_bar_item.dart';
import 'package:flutter_xiecheng/page/profile/my_page.dart';
import 'package:flutter_xiecheng/page/search/search_page.dart';
import 'package:flutter_xiecheng/page/travel/travel_page.dart';

final _defaultColor = Colors.grey;
final _activeColor = Colors.blue;

List<JKBottomBarItem> items = [
  JKBottomBarItem(Icon(Icons.home, color: _defaultColor), Icon(Icons.home, color: _activeColor), '首页'),
  JKBottomBarItem(Icon(Icons.search, color: _defaultColor), Icon(Icons.search, color: _activeColor), '搜索'),
  JKBottomBarItem(Icon(Icons.camera_alt, color: _defaultColor), Icon(Icons.camera_alt, color: _activeColor), '旅行'),
  JKBottomBarItem(Icon(Icons.account_circle, color: _defaultColor), Icon(Icons.account_circle, color: _activeColor), '我的')
];

List<Widget> pages = [
  HomePage(),
  SearchPage(),
  TravelPage(),
  MyPage()
];