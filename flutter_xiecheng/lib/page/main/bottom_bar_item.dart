
import 'package:flutter/material.dart';

class JKBottomBarItem extends BottomNavigationBarItem {

  JKBottomBarItem(Widget icon, Widget activeIcon, String title): super(
    label: title,
    icon: icon,
    activeIcon: activeIcon,
  );
}

/**
 * class JKBottomBarItem extends BottomNavigationBarItem {

    JKBottomBarItem(String iconName, String title): super(
    label: title,
    icon: Image.asset('assets/images/tabbar/$iconName.png', width: 30, gaplessPlayback: true,),
    activeIcon: Image.asset('assets/images/tabbar/${iconName}_active.png', width: 30, gaplessPlayback: true,),
    );
    }
*/