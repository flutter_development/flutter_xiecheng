import 'package:flutter/material.dart';
import 'package:flutter_sizefit/flutter_sizefit.dart';

/// 搜索的类型
enum SearchBarType {home, normal, homeLight}
// typedef EmptyRefreshBlock = Function(bool result);
/// 顶部搜索
class SearchBar extends StatefulWidget {
  /// 是否禁止搜索
  final bool enabled;
  /// 是否隐藏左边的按钮
  final bool hideLeft;
  /// 搜索类型
  final SearchBarType searchBarType;
  /// 默认的占位字(清空之后的文字)
  final String hintText;
  /// 默认的提示文案
  final String defaultText;
  /// 左边按钮的点击
  final void Function() leftButtonClick;
  /// 右边按钮的点击
  final void Function() rightButtonClick;
  /// 语音的点击
  final void Function() speakClick;
  /// 输入框的点击
  final void Function() inputBoxClick;
  /// 文字的变化
  final ValueChanged<String> onChange;

  const SearchBar({Key key, this.enabled = true, this.hideLeft, this.searchBarType = SearchBarType.normal, this.hintText, this.defaultText, this.leftButtonClick, this.rightButtonClick, this.speakClick, this.inputBoxClick, this.onChange}) : super(key: key);

  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {

  bool isShowClear = false;
  /// 输入框的监听
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.searchBarType != SearchBarType.normal) {
      isShowClear = false;
      return;
    }
    if(widget.defaultText != null && widget.defaultText.length > 0) {
      setState(() {
        isShowClear = true;
        _controller.text = widget.defaultText;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.searchBarType == SearchBarType.normal ? _normalSearchView() : _homeSearchView();
  }

  Widget _normalSearchView() {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          _wrapGesture(Container(
            padding: const EdgeInsets.fromLTRB(6, 5, 10, 5),
            child: widget.hideLeft ?? false ? null : const Icon(Icons.arrow_back_ios, color: Colors.grey,),
          ), widget.leftButtonClick),
          Expanded(
              flex: 1,
              child: _inputBox()
          ),
          _wrapGesture(Container(
            padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
            child: const Text("搜索", style: TextStyle(color: Colors.blue, fontSize: 17),),
          ), widget.rightButtonClick)
        ],
      ),
    );
  }

  Widget _homeSearchView() {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          _wrapGesture(Container(
            padding: const EdgeInsets.fromLTRB(6, 5, 10, 5),
            child: Row(
              children: [
                Text("上海", style: TextStyle(color: _homeFontColor(), fontSize: 14),),
                Icon(Icons.expand_more, color: _homeFontColor(), size: 22,)
              ],
            ),
          ), widget.leftButtonClick),
          Expanded(
              flex: 1,
              child: _inputBox()
          ),
          _wrapGesture(Container(
            padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
            child: Icon(Icons.comment, color: _homeFontColor(), size: 26,),
          ), widget.rightButtonClick)
        ],
      ),
    );
  }

  /// 首页的字体颜色
  _homeFontColor() {
    return widget.searchBarType == SearchBarType.homeLight ? Colors.black54 : Colors.white;
  }

  /// 输入框
  _inputBox() {
    Color inputBackGroundColor;
    if(widget.searchBarType == SearchBarType.home) {
      inputBackGroundColor = Colors.white;
    } else {
      inputBackGroundColor = Color(int.parse("0xffededed"));
    }

    print('状态栏的高度：${JKSizeFit.topStatusHeight} 导航栏的高度：${JKSizeFit.navigationBarHeight}');

    return Container(
      height: 44,
      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
      decoration: BoxDecoration(
          color: inputBackGroundColor,
          borderRadius: BorderRadius.circular(widget.searchBarType == SearchBarType.normal ? 5 : 15)
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // Color(0xA9A9A9)
          Container(
            child: Icon(Icons.search, color: widget.searchBarType == SearchBarType.normal ? Colors.red : Colors.blue, size: 20,),
          ),
          Expanded(
              flex: 1,
              child: widget.searchBarType == SearchBarType.normal ? TextField(
                  controller: _controller,
                  onChanged: _onChange,
                  autofocus: true,
                  style: const TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                    fontWeight: FontWeight.w300,
                  ),
                  decoration: InputDecoration(
                      //fillColor: Colors.brown,//背景颜色，必须结合filled: true,才有效
                      //filled: true,//重点，必须设置为true，fillColor才有效
                      contentPadding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                      border: InputBorder.none,
                      hintText: widget.hintText ?? "",
                      hintStyle: const TextStyle(fontSize: 14)
                  )
              ) : _wrapGesture(Text(widget.defaultText, style: const TextStyle(fontSize: 16, color: Colors.grey),), widget.inputBoxClick)
          ),
          isShowClear ? _wrapGesture(Icon(Icons.clear,size: 22, color: widget.searchBarType == SearchBarType.normal ? Colors.blue : Colors.grey,), () {
            setState(() {
              _controller.clear();
            });
            isShowClear = false;
            widget.onChange('');
          }) : _wrapGesture(Icon(Icons.mic,size: 22, color: Colors.grey), widget.speakClick)
        ],
      ),
    );
  }

  _wrapGesture(Widget widget, void Function() callBack) {
    return GestureDetector(
      onTap: () {
        if(callBack != null) callBack();
      },
      child: widget,
    );
  }

  /// 输入框的变化
  _onChange(String text) {
    print("我是💣💣💣💣💣💣💣💣💣");
    if(text.length > 0) {
      setState(() {
        isShowClear = true;
      });
    } else {
      setState(() {
        isShowClear = false;
      });
    }
    if(widget.onChange != null) {
      widget.onChange(text);
    }
  }
}
